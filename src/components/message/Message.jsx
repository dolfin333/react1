import moment from "moment"
import React from "react"
import "./Message.css"

class Message extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLiked: false
        }
    }
    getFormatedDate(message) {
        const date_ob = message.editedAt ? new Date(message.editedAt) : new Date(message.createdAt);
        return moment(date_ob).format("hh:mm")
    }
    render() {
        return (
            <div className="message">
                <div className="message-user-avatar">
                    <img src={this.props.message.avatar} alt="avatar" />
                </div>
                <div className="main-mess-info">
                    <div className="message-header">
                        <div className="message-user-name">
                            {this.props.message.user}
                        </div>
                        <button
                            className={this.state.isLiked ? "message-liked" : "message-like"}
                            onClick={() => { this.setState({ isLiked: !this.state.isLiked }) }}>
                            {this.state.isLiked ?
                                <img src="/full-heart.webp" alt="heart" /> :
                                <img src="/empty-heart.png" alt="heart" />}
                        </button>
                    </div>
                    <div className="message-text">
                        {this.props.message.text}
                    </div>
                    <div className="message-time">
                        {this.props.message.editedAt && "edited at "}
                        {this.getFormatedDate(this.props.message)}
                    </div>
                </div>

            </div >
        )
    }
}

export default Message;