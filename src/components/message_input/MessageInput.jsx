import React from "react"
import "./MessageInput.css"

class MessageInput extends React.Component {
    render() {
        return (
            <div className="message-input">
                <input value={this.props.text_input} className="message-input-text" onChange={(e) => {
                    this.props.setTextInput(e.target.value)
                }} />
                <button
                    className="message-input-button"
                    onClick={() => {
                        if (this.props.edited_message) {
                            this.props.updateMessage(this.props.edited_message.id)
                        } else {
                            this.props.addNewMessage();
                        }
                    }}
                >
                    {this.props.edited_message ? "Edit" : "Send"}
                </button>
                {this.props.edited_message &&
                    <button
                        className="message-cancel-button"
                        onClick={this.props.cancelUpdate}>
                        Cancel
                    </button>}
            </div>
        )
    }
}

export default MessageInput;