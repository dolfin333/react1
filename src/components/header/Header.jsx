import moment from "moment"
import React from "react"
import "./Header.css"

class Header extends React.Component {

    getDateFormat(last_message_date) {
        if (last_message_date !== "") {
            return moment(last_message_date).format("DD.MM.YYYY HH:mm")
        }
        return ""
    }
    render() {
        return (
            <div className="header">
                <div className="header-title">
                    Sweety chat
                </div>
                <div className="header-users-count-wrap">
                    <span>Users</span>
                    <div className="header-users-count">
                        {this.props.users_counter}
                    </div>
                </div>
                <div className="header-messages-count-wrap">
                    <span>Messages</span>
                    <div className="header-messages-count">
                        {this.props.message_counter}
                    </div>
                </div>
                <div className="header-last-message-date-wrap">
                    <span>Last message</span>
                    <div className="header-last-message-date">
                        {this.getDateFormat(this.props.last_message_date)}
                    </div>
                </div>
            </div>
        )
    }
}

export default Header