import moment from "moment";
import React from "react"
import Message from "../message/Message";
import OwnMessage from "../own_message/OwnMessage";
import "./MessageList.css"

class MessageList extends React.Component {
    groupday(array) {
        let byday = {};
        array.forEach(value => {
            let d = new Date(value.createdAt);
            d = Math.floor(d.getTime() / (1000 * 60 * 60 * 24));
            byday[d] = byday[d] || [];
            byday[d].push(value);
        })
        return byday
    }

    getDifferenceInDays(date1, date2) {
        const days = Math.floor((date2.getTime() - date1.getTime()) / (1000 * 3600 * 24));
        return days
    }
    getDate(date1, date2) {
        const diffDays = this.getDifferenceInDays(date1, date2)
        if (diffDays === 0) {
            return "Today"
        }
        if (diffDays === 1) {
            return "Yesterday"
        }
        return moment(date1).format("dddd, DD MMMM")
    }
    render() {
        const messages = this.groupday(this.props.messages);
        return (
            <div className="message-list">
                {
                    Object.keys(messages).map(key => (
                        <div key={messages[key][0].id + messages[key][0].userId}>
                            <div className="messages-divider">
                                <div className="messages-divider-date">
                                    {this.getDate(new Date(messages[key][0].createdAt), new Date())}
                                </div>
                            </div>
                            {messages[key].map(message => (
                                message.userId === this.props.own_user_id ?
                                    <OwnMessage
                                        message={message}
                                        key={message.id}
                                        deleteMessage={this.props.deleteMessage}
                                        editMessage={this.props.editMessage} /> :
                                    <Message message={message} key={message.id} />
                            ))
                            }
                        </div>
                    ))
                }
            </div>
        )
    }
}

export default MessageList;