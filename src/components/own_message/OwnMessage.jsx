import moment from "moment";
import React from "react"
import "./OwnMessage.css";

class OwnMessage extends React.Component {

    getFormatedDate(message) {
        const date_ob = message.editedAt ? new Date(message.editedAt) : new Date(message.createdAt)
        return moment(date_ob).format("hh:mm")
    }
    render() {
        return (
            <div className="own-message-wrap">
                <div className="own-message">
                    <div className="message-text">
                        {this.props.message.text}
                    </div>
                    <div className="message-time">
                        {this.props.message.editedAt && "edited at "}
                        {this.getFormatedDate(this.props.message)}
                    </div>
                    <div className="buttons">
                        <button
                            className="message-edit"
                            onClick={() => { this.props.editMessage(this.props.message.id) }}>
                            edit
                        </button>
                        <button
                            className="message-delete"
                            onClick={() => { this.props.deleteMessage(this.props.message.id) }}>
                            delete
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export default OwnMessage;