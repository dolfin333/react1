import React from "react"
import "./Chat.css"
import Preloader from "./preloader/Preloader";
import MessageInput from "./message_input/MessageInput";
import Header from "./header/Header";
import MessageList from "./message_list/MessageList";

class Chat extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            isLoading: true,
            last_message_date: null,
            message_counter: null,
            users_counter: null,
            usersMap: null,
            own_user_id: null,
            next_id: null,
            edited_message: null,
            text_input: ""
        }
    }

    async componentDidMount() {
        const messages = await (await fetch(this.props.url, { method: "GET" })).json();
        const usersMap = new Map();
        messages.forEach(message => {
            usersMap.set(message.user, typeof usersMap.get(message.user) !== 'undefined' ? (1 + usersMap.get(message.user)) : 1);
        })
        messages.sort((a, b) => {
            return new Date(a.createdAt) - new Date(b.createdAt)
        })
        this.setState({
            messages,
            isLoading: false,
            last_message_date: messages[messages.length - 1] ? messages[messages.length - 1].createdAt : "",
            message_counter: messages.length,
            users_counter: usersMap.size,
            usersMap: usersMap,
            own_user_id: "9e243930-83c9-11e9-8e0c-8f1a686f4ce5",
            next_id: 1,
        })
    }

    setTextInput(text) {
        this.setState({ ...this.state, text_input: text })
    }

    addNewMessage() {
        const message = {
            "id": this.state.next_id,
            "userId": this.state.own_user_id,
            "avatar": "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
            "user": "Liza",
            "text": this.state.text_input,
            "createdAt": new Date(),
            "editedAt": ""
        }
        const usersMap = this.state.usersMap
        const count = usersMap.get(message.user)
        usersMap.set(message.user, typeof count !== 'undefined' ? (1 + count) : 1);
        const messages = this.state.messages;
        messages.push(message)
        this.setState({
            ...this.state,
            messages: messages,
            next_id: this.state.next_id + 1,
            text_input: "",
            users_counter: usersMap.size,
            usersMap: usersMap,
            message_counter: this.state.message_counter + 1,
            last_message_date: message.createdAt
        })
    }
    deleteMessage(id) {
        const messages = this.state.messages;
        const index = messages.findIndex(mess => mess.id === id);
        const usersMap = this.state.usersMap;
        usersMap.set(messages[index].user, usersMap.get(messages[index].user) - 1);
        if (usersMap.get(messages[index].user) === 0) {
            usersMap.delete(messages[index].user);
        }
        messages.splice(index, 1)
        this.setState({
            ...this.state,
            messages: messages,
            message_counter: this.state.message_counter - 1,
            users_counter: usersMap.size,
            usersMap: usersMap,
            last_message_date: messages[messages.length - 1] ? messages[messages.length - 1].createdAt : ""
        })
    }

    editMessage(id) {
        const messages = this.state.messages;
        this.setState({
            ...this.state,
            edited_message: messages.find(mess => mess.id === id),
            text_input: messages.find(mess => mess.id === id).text
        })
    }

    cancelUpdate() {
        this.setState({
            ...this.state,
            edited_message: null,
            text_input: ""
        })
    }

    updateMessage(id) {
        const messages = this.state.messages;
        const message_id = messages.findIndex(mess => mess.id === id);
        if (messages[message_id].text !== this.state.text_input) {
            messages[message_id].text = this.state.text_input;
            messages[message_id].editedAt = new Date();
        }
        this.setState({
            ...this.state,
            messages: messages,
            edited_message: null,
            text_input: ""
        })
    }
    render() {
        return (
            <div className="chat">
                {this.state.isLoading ? (
                    <Preloader />
                ) : (
                    <div>
                        <Header
                            message_counter={this.state.message_counter}
                            users_counter={this.state.users_counter}
                            last_message_date={this.state.last_message_date} />
                        <MessageList
                            messages={this.state.messages}
                            own_user_id={this.state.own_user_id}
                            deleteMessage={this.deleteMessage.bind(this)}
                            editMessage={this.editMessage.bind(this)}
                        />
                    </div>
                )
                }
                <MessageInput
                    addNewMessage={this.addNewMessage.bind(this)}
                    edited_message={this.state.edited_message}
                    updateMessage={this.updateMessage.bind(this)}
                    setTextInput={this.setTextInput.bind(this)}
                    cancelUpdate={this.cancelUpdate.bind(this)}
                    text_input={this.state.text_input} />

            </div>
        )
    }
}

export default Chat